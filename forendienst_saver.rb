﻿#!/usr/bin/env ruby
# encoding: UTF-8
#
# A short program to fetch the contents of a forendienst board and
# save them into a sqlite database for further use. 
# Only content to which the specified user has access can be read.
#
# Copyright (C) 2013 Sebastian Hugentobler <sebastian@nulda.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'logger'
require 'mechanize'
require 'json'
require 'sqlite3'
require 'trollop'

class ForendienstSaver
    def initialize
        @agent = Mechanize.new        
        @agent.user_agent_alias = 'Mac Safari'

        @logger = Logger.new(STDOUT)        
    end
    
    def login(boardnr, username, password)
        @boardnr = boardnr
        page = @agent.get "http://#{boardnr}.forendienst.de/login.php?login_origin="
        login_form = page.forms[0]
        
        login_form.username = username
        login_form.password = password
        login_form.add_field!('submit', value = 'Login')
        
        login_result = @agent.submit login_form
        login_result.body.include? 'Du hast dich erfolgreich eingeloggt.'
    end
    
    def find_max_page(boardurl)
        page_source = @agent.get boardurl 
        pageinfo = page_source.parser.xpath("(//table[@cellspacing='1'])[position()=1]/tr/td/p/a")        
        pageinfo[-2].nil? ? 1 : Integer(pageinfo[-2].text)
    end
    
    def find_boards
        page_source = @agent.get "http://#{@boardnr}.forendienst.de/index.php"      
        table = page_source.parser.xpath('(//table/tr/td/table)[position()=1]')
        
        groups_data = table.xpath("./tr[@class='tdsubhead']")        
        groups = groups_data.map{ |data| data.text }

        subboards_data = table.xpath("./tr[contains(@class, 'row')]/td[position()=2]")
        subboards_data.map{ |data| 
            @logger.debug "fetched board #{data.xpath('./a').text}"
            { 'link' => data.xpath('./a').attr('href').text,
              'title' => data.xpath('./a').text,
              'description' => data.xpath('./p').text }            
        } 
    end
    
    def find_topics(boardurl)
        page_source = @agent.get boardurl        
        topic_data = page_source.parser.xpath("//tr[contains(@class, 'row')]/td[position()=2]")
        
        topics = topic_data.map{ |data|
            title = data.xpath('./strong') ? "#{data.xpath('./strong').text} #{data.xpath('./a')[0].text}" : data.xpath('./a').text           
            @logger.debug "fetched topic #{title}"
            { 'link' => data.xpath('./a').attr('href').text,
              'title' => title }
        }
    end
    
    def find_topics_base(board)
        lastpage = find_max_page "http://#{@boardnr}.forendienst.de/#{board['link']}"
        topics = []
        lastpage.times do |page|
            pageurl = "http://#{@boardnr}.forendienst.de/#{board['link']}&page=#{page + 1}"            
            topics = (topics << find_topics(pageurl)).flatten
        end        
        
        return topics
    end   
    
    def find_posts(topicurl)
        page_source = @agent.get topicurl
        
        post_data = page_source.parser.xpath("//tr[contains(@class, 'profrow')]")
        
        post_data.map {|data|            
            userinfo = data.xpath('(./td)[position()=1]')
            postinfo = data.xpath('(./td)[position()=2]') 
            
            username = userinfo.xpath('(./p)[position()=1]').text 
            
            if username.include? '(M+)'
                username = username.gsub('(M+)', '').strip!
            end
            
            if username.include? '(M)'
                username = username.gsub('(M)', '').strip!
            end
            
            titleinfo = postinfo.xpath('(./table/tr/td)[position()=1]').text.split(':')                                    
            date = DateTime.strptime(titleinfo[0..1].join(':').strip, '%d.%m.%Y %H:%M')
            title = titleinfo[2..-1].join(':').strip
           
            post = postinfo.xpath('(./table/tr/td)[position()=3]').inner_html.gsub('<br>', '<br/>').strip
            
            signature_seperator = '______________'
            if post.include? signature_seperator
                post_data = post.split(signature_seperator)
                
                if post_data.length > 2
                    post = post_data[0..-2].join('')
                else
                    post = post_data[0]
                end                
            end
            
            { 'creator' => username,
              'date' => date,
              'title' => title,
              'post' => post }
        }        
    end
    
    def find_posts_base(topic)
        lastpage = find_max_page "http://#{@boardnr}.forendienst.de/#{topic['link']}"
        
        posts = []
        lastpage.times do |page|
            pageurl = "http://#{@boardnr}.forendienst.de/#{topic['link']}&page=#{page + 1}"            
            posts = (posts << find_posts(pageurl)).flatten
        end
        @logger.debug "fetched posts for topic #{topic['title']}"
        
        return posts
    end
    
    def find_users(user_link)
        page_source = @agent.get user_link
        user_data = page_source.parser.xpath("//tr[contains(@class, 'row')]/td[position()=2]")
        
        
        users = user_data.map { |data|            
            user_link = data.xpath('./a').attr('href').text
            user_name = data.xpath('./a').text
            
            user_info = find_user_details(user_link)
            user_info['link'] = user_link
            user_info['name'] = user_name
            
            @logger.debug "fetched user #{user_name}"
            
            user_info          
        }
    end
    
    def find_users_base
        page_source = @agent.get "http://#{@boardnr}.forendienst.de/member.php"
        lastpage = Integer(page_source.parser.xpath("//p/b[text()='Seiten:']/ancestor::p[1]/a")[-2].text)
      
        users = []
        lastpage.times do |page|
            pageurl = "http://#{@boardnr}.forendienst.de/member.php?sort=uid&type=asc&page=#{page + 1}"            
            users = (users << find_users(pageurl)).flatten
        end
        
        return users
    end
    
    def find_user_details(user_link)
        page_source = @agent.get "http://#{@boardnr}.forendienst.de/#{user_link}"
        user_detail_data = page_source.parser.xpath("//tr[@class='row1']/ancestor::table[1]/tr[position()=2]/td[position()=2]/div/table")
        
        registered_at = DateTime.strptime(user_detail_data.xpath("./tr[position()=1]/td[position()=2]").text, '%d.%m.%Y %H:%M')
        supermod = user_detail_data.xpath("./tr[position()=3]/td[position()=2]").text == 'ja'
        
        birthstring = user_detail_data.xpath("./tr[position()=4]/td[position()=2]").text        
        birthdate = (birthstring.nil? or birthstring.empty?) ? '' : Date.strptime(birthstring, '%d.%m.%Y')
        
        homepage_data = user_detail_data.xpath("./tr[position()=5]/td[position()=2]/a[position()=1]")[0]
        
        homepage = homepage_data.nil? ? '' : homepage_data.attr('href')       
        interests = user_detail_data.xpath("./tr[position()=6]/td[position()=2]").text
        fave_country = user_detail_data.xpath("./tr[position()=7]/td[position()=2]").text
        fave_hero_type = user_detail_data.xpath("./tr[position()=8]/td[position()=2]").text
        fave_spell = user_detail_data.xpath("./tr[position()=9]/td[position()=2]").text  

        { 'register_date' => registered_at,
          'supermod' => supermod,
          'birthdate' => birthdate,
          'homepage' => homepage,
          'interests' => interests,
          'fave_country' => fave_country,
          'fave_hero_type' => fave_hero_type,
          'fave_spell' => fave_spell,
          'signature' => find_user_signature(user_link) }
    end
    
    def find_user_signature(user_link)
        page_source = @agent.get "http://#{@boardnr}.forendienst.de/#{user_link}"
        user_detail_data = page_source.parser.xpath("//tr[@class='row1']/ancestor::table[1]/tr[position()=2]/td[position()=2]/div/table")
        
        user_posts = user_detail_data.xpath("./tr[position()=2]/td[position()=2]/a").first
        if user_posts.nil? then return '' end
        
        user_posts_link = user_posts.attr('href')
        
        page_source = @agent.get "http://#{@boardnr}.forendienst.de/#{user_posts_link}"        
        first_post_link = page_source.parser.xpath("//tr[@class='row1'][position()=1]/td[position()=1]/a").attr('href').text
        
        post_source = @agent.get "http://#{@boardnr}.forendienst.de/#{first_post_link}"
        post = post_source.parser.xpath("//tr[contains(@class, 'profrow')]/td/a[@name='#{first_post_link.split('#')[-1]}']/ancestor::tr[1]/td[position()=2]/table/tr[position()=3]/td").inner_html
        
        signature_split = post.split('______________')        
        if signature_split.length < 2 then return '' end        
        
        signature = post.split('______________')[-1].sub('<br>', '').sub('<br>', '').gsub('<br>', '<br/>')
    end
    
    def get_forum(forumid, username, password)
        login(forumid, username, password)
        users = find_users_base
        boards = find_boards   
        boards.each { |board| board['topics'] = find_topics_base(board) } 
        boards.each { |board| board['topics'].each { |topic| topic['posts'] = find_posts_base(topic) }}
       
        {'users' => users, 'boards' => boards }
    end
end

class ForendienstDbSaver
    def initialize(dbfile)
        @dbfile = dbfile        
        @logger = Logger.new(STDOUT)
        
        initialize_db
    end

    def initialize_db
        begin
            db = SQLite3::Database.new @dbfile
            
            @logger.debug 'Creating tables...'
            
            db.execute 'DROP TABLE IF EXISTS users'
            db.execute 'CREATE TABLE users (name TEXT PRIMARY KEY, register_date DATETIME NOT NULL, super_mod BOOLEAN, birth_date DATE, website TEXT, interests TEXT, fave_country TEXT, fave_hero_type TEXT, fave_spell TEXT, signature TEXT)'
            
            db.execute 'DROP TABLE IF EXISTS boards'
            db.execute 'CREATE TABLE boards (id INTEGER PRIMARY KEY, title TEXT NOT NULL, description TEXT)'
            
            db.execute 'DROP TABLE IF EXISTS topics'
            db.execute 'CREATE TABLE topics (id INTEGER PRIMARY KEY, title TEXT NOT NULL, parent_board_id INTEGER NOT NULL, FOREIGN KEY(parent_board_id) REFERENCES boards(id))'
            
            db.execute 'DROP TABLE IF EXISTS posts'
            db.execute 'CREATE TABLE posts (id INTEGER PRIMARY KEY, title TEXT NOT NULL, body TEXT NOT NULL, create_date DATETIME NOT NULL, creator TEXT NOT NULL, parent_topic_id INTEGER NOT NULL, FOREIGN KEY(creator) REFERENCES users(name), FOREIGN KEY(parent_topic_id) REFERENCES topics(id))'

        rescue SQLite3::Exception => e     
            @logger.fatal e 
            raise e  
        ensure
            db.close if db
        end
    end

    def generate_sqlite(board_data)
        create_users board_data['users']
        create_boards board_data['boards']        
    end
    
    def create_users(users)
         begin            
            db = SQLite3::Database.open @dbfile           
            user_insert = db.prepare 'INSERT INTO users(name, register_date, super_mod, birth_date, website, interests, fave_country, fave_hero_type, fave_spell, signature) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
            users.each do |user| 
                @logger.debug "creating user #{user['name']}"
                
                user_insert.execute(
                    user['name'],
                    user['register_date'],
                    user['supermod'] ? 1 : 0,
                    user['birthdate'].empty? ? 'NULL' : user['birthdate'],
                    user['homepage'],
                    user['interests'],
                    user['fave_country'],
                    user['fave_hero_type'],
                    user['fave_spell'],
                    user['signature'])
            end
        rescue SQLite3::Exception => e     
            @logger.fatal e
            raise e   
        ensure
            user_insert.close if user_insert
            db.close if db
        end
    end
    
    def create_boards(boards)
        begin            
            db = SQLite3::Database.open @dbfile
            board_insert = db.prepare 'INSERT INTO boards(title, description) VALUES (?, ?)'
            boards.each do |board|   
                @logger.debug "creating board #{board['title']}"
                
                board_insert.execute(
                    board['title'],
                    board['description']
                )
                create_topics board, db.last_insert_row_id
            end
        rescue SQLite3::Exception => e     
            @logger.fatal e  
            raise e 
        ensure
            board_insert.close if board_insert
            db.close if db
        end
    end
    
    def create_topics(board, board_parent_id)
         begin
            db = SQLite3::Database.open @dbfile            
            topic_insert = db.prepare 'INSERT INTO topics(title, parent_board_id) VALUES(?, ?)'
            board['topics'].each do |topic| 
                @logger.debug "creating topic #{topic['title']}"
                
                topic_insert.execute(
                    topic['title'],
                    board_parent_id
                )
                create_posts topic, db.last_insert_row_id
            end
        rescue SQLite3::Exception => e     
            @logger.fatal e
            raise e    
        ensure
            topic_insert.close if topic_insert
            db.close if db
        end
    end
    
    def create_posts(topic, topic_parent_id)
         begin
            db = SQLite3::Database.open @dbfile            
            post_insert = db.prepare 'INSERT INTO posts(title, body, create_date, creator, parent_topic_id) VALUES(?, ?, ?, ?, ?)'
            topic['posts'].each do |post| 
                @logger.debug "creating post #{post['title']} by #{post['creator']}"
                
                post_insert.execute(
                    post['title'],
                    post['post'],
                    post['date'],
                    post['creator'],
                    topic_parent_id 
                )
            end
        rescue SQLite3::Exception => e     
            @logger.fatal e
            raise e    
        ensure
            post_insert.close if post_insert
            db.close if db
        end
    end
end

if __FILE__ == $0
    opts = Trollop::options do
        version 'forendienst saver (c) 2013 Sebastian Hugentobler <sebastian@nulda.ch>'
        banner <<-EOS
forendienst saver (c) 2013 Sebastian Hugentobler <sebastian@nulda.ch>

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it
under certain conditions; read the 'COPYING' file for details.

options:
     
EOS
        opt :boardid, 'Forendienst board id', :type => :integer, :required => true
        opt :user, 'Username to login', :type => :string, :required => true       
        opt :password, 'Password to login', :type => :string, :required => true
        opt :output, 'Path for the sqlite output without file extension', :type => :string, :required => true
        opt :generate_json, 'Generate an additional json data dump', :default => false
        opt :read_json, 'Data is fetched from a specified json dump', :type => :string
    end
   
    if not opts[:read_json].nil? and File.file? opts[:read_json]
        board_data = JSON.parse(IO.read(opts[:read_json]).force_encoding('utf-8'))
    else    
        saver = ForendienstSaver.new    
        board_data = saver.get_forum opts[:boardid], opts[:user], opts [:password]
    end
    
    if opts[:generate_json]
        File.open("#{opts[:output]}.json", 'w') { |file| file.write board_data.to_json }
    end
    
    db_saver = ForendienstDbSaver.new "#{opts[:output]}.sqlite"
    db_saver.generate_sqlite board_data
end
